# translation of old-stuff.po to Russian
# Sergey Alyoshin <alyoshin.s@gmail.com>, 2009.
# Александр <antsev@tula.net>, 2011.
# Yuri Kozlov <yuray@komyakino.ru>, 2006, 2007, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2008, 2009, 2011, 2013.
# Lev Lamberov <l.lamberov@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2017-06-16 23:30+0200\n"
"PO-Revision-Date: 2017-06-15 12:55+0500\n"
"Last-Translator: Lev Lamberov <l.lamberov@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "ru"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Подготовка системы &oldreleasename; к обновлению"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"В этом приложении содержится информация о том, как перед обновлением до "
"&releasename; удостовериться, что вы можете устанавливать или обновлять "
"пакеты &oldreleasename;. Это может понадобиться в некоторых случаях."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Обновление системы &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"В основном, это обновление ничем не отличается от всех предыдущих обновлений "
"&oldreleasename;, которые вы делали. Единственное отличие состоит в том, что "
"сначала надо убедиться, что ваши списки пакетов всё ещё содержат ссылки на "
"пакеты &oldreleasename;, как описано в <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Если вы обновляете систему с сервера-зеркала Debian, то автоматически будет "
"выполнено обновление до последнего выпуска &oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your sources list"
msgstr "Проверка списка источников APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your <filename>/etc/apt/sources.list</filename> refer "
"to <quote><literal>stable</literal></quote>, it effectively points to "
"&releasename; already. This might not be what you want if you are not ready "
"yet for the upgrade.  If you have already run <command>apt-get update</"
"command>, you can still get back without problems by following the procedure "
"below."
msgstr ""
"Если в какой-либо строке в вашем файле <filename>/etc/apt/sources.list</"
"filename> имеется ссылка стабильный дистрибутив (<quote><literal>stable</"
"literal></quote>), то она указывает на &releasename;. Это может быть не то, "
"что вы хотите, если вы пока не готовы к обновлению. Если вы уже запустили "
"<command>apt-get update</command>, то ещё не поздно всё отменить, просто "
"выполнив инструкцию, приведённую ниже."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Если вы успели установить пакеты из &releasename;, то особого смысла в "
"установке пакетов из &oldreleasename; уже нет. В этом случае вам следует "
"решить &mdash; доводить до конца обновление или нет. Вернуться к "
"использованию старых версий пакетов возможно, но эта процедура выходит за "
"рамки данного документа."

#. type: Content of: <appendix><section><para><footnote><para>
#: en/old-stuff.dbk:52
msgid ""
"<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
"\">Debian will remove FTP access to all of its official mirrors on "
"2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
"literal> host, please consider switching to <ulink url=\"https://deb.debian."
"org\">deb.debian.org</ulink>.  This note only applies to mirrors hosted by "
"Debian itself.  If you use a secondary mirror or a third-party repository, "
"then they may still support FTP access after that date.  Please consult with "
"the operators of these if you are in doubt."
msgstr ""
"<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
"\">Debian закроет доступ по FTP ко всем официальным зеркалам 2017-11-01</"
"ulink>. Если в вашем файле sources.list содержится узел <literal>debian.org</"
"literal>, рекомендуется перейти на использование <ulink url=\"https://deb."
"debian.org\">deb.debian.org</ulink>. Это касается только зеркал, "
"поддерживаемых непосредственно Debian. Если вы используете вторичное зеркало "
"или репозиторий третьей стороны, то на них после указанной даты всё ещё "
"может сохраняться поддержка FTP. В случае сомнения за дополнительной "
"информацией обратитесь к операторам используемых вами зеркал."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"Open the file <filename>/etc/apt/sources.list</filename> with your favorite "
"editor (as <literal>root</literal>) and check all lines beginning with "
"<literal>deb http:</literal>, <literal>deb https:</literal>, <literal>deb tor"
"+http:</literal>, <literal>deb tor+https:</literal> or <literal>deb ftp:</"
"literal><placeholder type=\"footnote\" id=\"0\"/> for a reference to "
"<quote><literal>stable</literal></quote>.  If you find any, change "
"<literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Откройте файл <filename>/etc/apt/sources.list</filename> с помощью вашего "
"любимого текстового редактора (от лица <literal>суперпользователя</literal>) "
"и проверьте все строки, начинающиеся с <literal>deb http:</literal>, "
"<literal>deb https:</literal>, <literal>deb tor+http:</literal>, "
"<literal>deb tor+https:</literal> или <literal>deb ftp:</"
"literal><placeholder type=\"footnote\" id=\"0\"/>, на наличие слова "
"<quote><literal>stable</literal></quote>. При нахождении, замените "
"<literal>stable</literal> на <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><note><para>
#: en/old-stuff.dbk:69
msgid ""
"Lines in sources.list starting with <quote>deb ftp:</quote> and pointing to "
"debian.org addresses should be changed into <quote>deb http:</quote> lines.  "
"See <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."
msgstr ""
"Строки в файле sources.list, начинающиеся с <quote>deb ftp:</quote> и "
"указывающие на адреса debian.org, следует заменить на строки вида <quote>deb "
"http:</quote>. Смотрите <xref linkend=\"deprecation-of-ftp-apt-mirrors\" />."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:75
msgid ""
"If you have any lines starting with <literal>deb file:</literal>, you will "
"have to check for yourself if the location they refer to contains an "
"&oldreleasename; or a &releasename; archive."
msgstr ""
"Если вы нашли строки, начинающиеся с <literal>deb file:</literal>, то вам "
"придётся самостоятельно проверить, какие пакеты хранятся в указанном "
"каталоге &mdash; &oldreleasename; или &releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:81
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
"Doing so would invalidate the line and you would have to run <command>apt-"
"cdrom</command> again.  Do not be alarmed if a <literal>cdrom:</literal> "
"source line refers to <quote><literal>unstable</literal></quote>.  Although "
"confusing, this is normal."
msgstr ""
"Не исправляйте строки, начинающиеся с <literal>deb cdrom:</literal>. Если вы "
"исправите такую строку, то вам придётся снова запустить команду <command>apt-"
"cdrom</command>. Не беспокойтесь, если источник cdrom ссылается на "
"нестабильный (<quote><literal>unstable</literal></quote>) выпуск. Как это ни "
"странно, так и должно быть."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:89
msgid "If you've made any changes, save the file and execute"
msgstr ""
"Если вы внесли какие-нибудь изменения, сохраните файл и выполните команду"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:92
#, no-wrap
msgid "# apt-get update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:95
msgid "to refresh the package list."
msgstr "для обновления списка пакетов."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:100
msgid "Removing obsolete configuration files"
msgstr "Удаление устаревших файлов настройки"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:102
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Перед обновлением системы до &releasename;, рекомендуется удалить из системы "
"старые файлы настроек (такие как <filename>*.dpkg-{new,old}</filename> в "
"<filename>/etc</filename>."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:110
msgid "Upgrade legacy locales to UTF-8"
msgstr "Переход к использованию локалей с UTF-8"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:112
msgid ""
"Using a legacy non-UTF-8 locale has been unsupported by desktops and other "
"mainstream software projects for a long time. Such locales should be "
"upgraded by running <command>dpkg-reconfigure locales</command> and "
"selecting a UTF-8 default. You should also ensure that users are not "
"overriding the default to use a legacy locale in their environment."
msgstr ""
"Использование устаревшей локали (не-UTF-8) уже долгое время не "
"поддерживается окружениями рабочего стола и другим широко используемым ПО. "
"Такие локали следует обновить, запустив команду <command>dpkg-reconfigure "
"locales</command> и выбрав по умолчанию UTF-8. Также вам следует убедиться, "
"что ваши пользователи не изменяют настройки по умолчанию и не используют "
"устаревшие варианты локали в своих окружениях."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "Хранитель экрана GNOME при использовании паролей с не-ASCII символами, "
#~ "поддержка pam_ldap или даже возможность разблокировать экран могут "
#~ "работать ненадёжно, если не используется UTF-8. Программа чтения с экрана "
#~ "GNOME содержит ошибку <ulink url=\"http://bugs.debian."
#~ "org/599197\">#599197</ulink>. Файловый менеджер Nautilus (и все программы "
#~ "на основе glib, и, вероятно, также все программы на Qt) предполагают, что "
#~ "имена файлов хранятся в UTF-8, хотя командная оболочка предполагает, что "
#~ "для этого используется кодировка текущей локали. При повседневном "
#~ "использовании с именами файлов не-ASCII просто невозможно работать. Кроме "
#~ "того, для программы чтения с экрана gnome-orca (которая позволяет "
#~ "работать с окружением рабочего стола GNOME незрячим пользователям) "
#~ "требуется локаль UTF-8 начиная с выпуска Squeeze; при использовании "
#~ "устаревшего набора символов невозможно прочитать информацию из окна таких "
#~ "элементов рабочего стола, как Nautilus/GNOME Panel или меню Alt-F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Если вы работаете с локализованной версией системы и используемая локаль "
#~ "основана не на UTF-8, то настоятельно советуем задействовать в системе "
#~ "локаль с UTF-8. В прошлом были ошибки<placeholder type=\"footnote\" id="
#~ "\"0\"/>, которые проявляются только, когда используется локаль не-UTF-8. "
#~ "Работа приложений для рабочего стола с этими устаревшими локалями "
#~ "поддерживается специфическими внутренними механизмами библиотек, и мы "
#~ "больше не можем предоставить полную поддержку пользователям, которые "
#~ "продолжают их использовать."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "Для настройки локали в системе запустите <command>dpkg-reconfigure "
#~ "locales</command>. Выберите локаль с UTF-8, когда будет задан вопрос о "
#~ "системной локали по умолчанию. Также, посмотрите настройки локали "
#~ "пользователей и убедитесь, что в их окружении не включены старые локали."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "Начиная с выпуска 2:1.7.7-12, xorg-server больше не читает файл "
#~ "XF86Config-4. Смотрите также <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
