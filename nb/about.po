# translation of about.po to Norwegian bokmål
# SOME DESCRIPTIVE TITLE
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Klaus Ade Johnstad <klaus@skolelinux.no>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: about\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2009-03-30 23:10+0200\n"
"Last-Translator: Klaus Ade Johnstad <klaus@skolelinux.no>\n"
"Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "nb"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Innledning"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
#, fuzzy
#| msgid ""
#| "This document informs users of the &debian; distribution about major "
#| "changes in version &release; (codenamed \"&releasename;\")."
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Dette dokumentet informerer brukerene av  &debian; distribusjonen om de "
"største endringene i versjonen  &release; (kodenavn  <quote>&releasename;</"
"quote>)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Dette dokumentet forklarer hvordan man på en sikker måte oppgraderer fra "
"versjon &oldrelease; (kodenavn &oldreleasename;) til den aktuelle versjonen, "
"og informerer også om de vanligste problemene man kan støte på."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>.  If in doubt, check the date on the first page to "
"make sure you are reading a current version."
msgstr ""
"Du finner den siste aktuelle versjonen av disse notatene på  <ulink url="
"\"&url-release-notes;\"></ulink>. Hvis du er usikker på om du har den siste "
"aktuelle versjonen, så sjekk datoen på forsiden."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:27
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Det er umulig for oss å ta med alle ting som kan gå galt, så derfor har vi "
"bare tatt med et utvalg basert på kombinasjonen av hyppigst forekommende og "
"alvorlighet."

#. type: Content of: <chapter><para>
#: en/about.dbk:33
#, fuzzy
#| msgid ""
#| "Please note that we only support and document upgrading from the previous "
#| "release of Debian (in this case, the upgrade from &oldrelease;).  If you "
#| "need to upgrade from older releases, we suggest you read previous "
#| "editions of the release notes and upgrade to &oldrelease; first."
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Merk at vi kun støtter og dokumenterer hvordan man oppgraderer fra den "
"forrige stabile versjonen av Debian  (i dette tilfellet er det "
"&oldrelease;). Hvis du vil oppgradere fra en enda eldre versjoner så "
"foreslår vi at du leser tidligere dokumentasjon på hvordan man oppgraderer "
"versjonene i mellom, og begynner med å oppgradere til &oldrelease; først"

#. type: Content of: <chapter><section><title>
#: en/about.dbk:41
msgid "Reporting bugs on this document"
msgstr "Rapportere feil i dette dokumentet"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:43
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Vi har forsøkt å teste alle skritt i dette dokumentet, vi har også forsøkt å "
"beskrive alle mulige feil som kan oppstå når man oppgraderer."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:48
#, fuzzy
#| msgid ""
#| "Nevertheless, if you think you have found a bug (incorrect information or "
#| "information that is missing)  in this documentation, please file a bug in "
#| "the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
#| "<systemitem role=\"package\">release-notes</systemitem> package."
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"want to review first the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Hvis du finner en feil (enten feil informasjon eller manglende informasjon) "
"i dette dokumentet, så vær  vennlig å rapporter dette som en feil i  <ulink "
"url=\"&url-bts;\"> feilrapportsystemet</ulink> mot pakken som heter "
"<systemitem role=\"package\">release-notes</systemitem>."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:60
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/about.dbk:68
msgid "Contributing upgrade reports"
msgstr "Bidra med oppgraderingsrapporter "

#. type: Content of: <chapter><section><para>
#: en/about.dbk:70
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Vi tar gjerne i mot informasjon fra dere brukere angående hvordan det gikk "
"når dere oppgraderte fra &oldreleasename; til &releasename;. Hvis dere er "
"villige til å dele denne informasjonen, så rapportere dette i <ulink url="
"\"&url-bts;\"> feilrapportsystemet</ulink> mot pakken <systemitem role="
"\"package\">upgrade-reports</systemitem> . Vi ber deg å komprimere "
"eventuelle vedlegg med kommandoen <command>gzip</command>."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:79
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Vennligst inkluder følgende informasjon når du sender inn en "
"oppgraderingsrapport:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:86
#, fuzzy
#| msgid ""
#| "The status of your package database before and after the upgrade: "
#| "<command>dpkg</command>'s status database available at <filename>/var/lib/"
#| "dpkg/status</filename> and <command>aptitude</command>'s package state "
#| "information, available at <filename>/var/lib/aptitude/pkgstates</"
#| "filename>.  You should have made a backup before the upgrade as described "
#| "at <xref linkend=\"data-backup\"/>, but you can also find backups of this "
#| "information in <filename>/var/backups</filename>."
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"Tilstanden på din pakkedatabase før og etter oppgraderingen: <command>dpkg</"
"command> sin status er tilgjengelig i <filename>/var/lib/dpkg/status</"
"filename> og <command>aptitude</command> sin status er tilgjengelig i "
"<filename>/var/lib/aptitude/pkgstates</filename>. Du bør ta en "
"sikkerhetskopi før oppgraderingen som beskrevet i <xref linkend=\"data-backup"
"\"/>, det skal også ligge en kopi i  <filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:99
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Sesjonslogger fra  <command>script</command>, som beskrevet i <xref linkend="
"\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:105
#, fuzzy
#| msgid ""
#| "Your <systemitem role=\"package\">apt</systemitem> logs, available at "
#| "<filename>/var/log/apt/term.log</filename> or your <command>aptitude</"
#| "command> logs, available at <filename>/var/log/aptitude</filename>."
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Ditt system sine <systemitem role=\"package\">apt</systemitem> logger, "
"tilgjengelig i <filename>/var/log/apt/term.log</filename> eller loggene til "
"<command>aptitude</command>, tilgjengelig i <filename>/var/log/aptitude</"
"filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:114
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Du bør passe på å ikke inkludere sensitiv eller konfidensiell informasjon i "
"dine loggfiler i en slik feilrapport, ettersom slike feilrapporter blir "
"gjort offentlig tilgjengelig."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:123
msgid "Sources for this document"
msgstr "Kilder brukt i dette dokumentet"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:125
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the SVN repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access the SVN please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"SVN information pages</ulink>."
msgstr ""
"Kilden til dette dokumentet er i  DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. HTML versjonen er lagd med <systemitem "
"role=\"package\">docbook-xsl</systemitem> og <systemitem role=\"package"
"\">xsltproc</systemitem>. PDF versjonen er lagd med <systemitem role="
"\"package\">dblatex</systemitem> eller <systemitem role=\"package\">xmlroff</"
"systemitem>. Kilden til Release Notes er tilgjengelig i SVN hos "
"<emphasis>Debian Documentation Project</emphasis>. Du kan bruke <ulink url="
"\"&url-vcs-release-notes;\">web interface</ulink> for å få tilgang til "
"enkeltfilene på nettet for å se på endringer i dem. For mer informasjon om "
"hvordan du bruker SVN ta en titt på <ulink url=\"&url-ddp-vcs-info;\">Debian "
"Documentation Project SVN information pages</ulink>."

#~ msgid ""
#~ "\n"
#~ "TODO: check status of #494028 about apt-get vs. aptitude\n"
#~ "TODO: any more things to add here?\n"
#~ msgstr ""
#~ "\n"
#~ "Husk: Sjekk status på #494028 angående apt-get vs. aptitude\n"
#~ "Husk: Er det flere ting å huske på?\n"
